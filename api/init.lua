
-- Some code below is from https://github.com/BuckarooBanzay/mapblock_lib (Copyright MIT)
--
-- All we get from all this is camps.save_area and camps.load_area
--
-- camps.save_area given 2 positions returns a 100% serializable table
--
-- camps.load_area given 2 positions and the data (from running camps.save_area or from serialized string of it)

-- collect nodes with on_timer attributes
local nodes_with_timer = {}
minetest.register_on_mods_loaded(function()
	for _,node in pairs(minetest.registered_nodes) do
		if node.on_timer then
			nodes_with_timer[node.name] = true
		end
	end
end)

-- checks if a table is empty
local function is_empty(tbl)
	return not tbl or not next(tbl)
end

camps.save_area = function (pos1, pos2)
	local rc = camps.range_vecs(pos1, pos2)
	pos1 = rc.min
	pos2 = rc.max

	-- prepare data structure
	local data = {
		air_only = true,
		nodes = {},
		param1 = {},
		param2 = {},
		metadata = nil
	}

	local timers = {}

	-- loop over all blocks and fill nodes, param1 and param2
	for z=pos1.z,pos2.z do
		for y=pos1.y,pos2.y do
			for x=pos1.x,pos2.x do
				local pos = {x=x, y=y, z=z}
				local node = minetest.get_node_or_nil(pos)
				if node == nil then
					node = {name="air", param1=nil, param2=nil}
				else
					if node.name == "ignore" then
						-- replace ignore blocks with air
						node.name = "air"
					end

					if data.air_only and node.name ~= "air" then
						-- mapblock contains not just air
						data.air_only = false
					end

					if nodes_with_timer[node.name] then
						-- node has a node-timer
						local timer = minetest.get_node_timer(pos)
						local relative_pos = vector.subtract(pos, pos1)
						if timer:is_started() then
							timers[minetest.pos_to_string(relative_pos)] = {
								timeout = timer:get_timeout(),
								elapsed = timer:get_elapsed()
							}
						end
					end
				end

				table.insert(data.nodes, node.name)
				table.insert(data.param1, node.param1)
				table.insert(data.param2, node.param2)
			end
		end
	end

	if not data.air_only then
		-- serialize metadata
		local pos_with_meta = minetest.find_nodes_with_meta(pos1, pos2)
		for _, mpos in ipairs(pos_with_meta) do
			local relative_pos = vector.subtract(mpos, pos1)
			local meta = minetest.get_meta(mpos):to_table()

			-- Convert metadata item stacks to item strings
			for _, invlist in pairs(meta.inventory) do
				for index = 1, #invlist do
					local itemstack = invlist[index]
					if itemstack.to_string then
						invlist[index] = itemstack:to_string()
					end
				end
			end

			-- re-check if metadata actually exists (may happen with minetest.find_nodes_with_meta)
			if not is_empty(meta.fields) or not is_empty(meta.inventory) then
				data.metadata = data.metadata or {}
				data.metadata.meta = data.metadata.meta or {}
				data.metadata.meta[minetest.pos_to_string(relative_pos)] = meta
			end

			if not is_empty(timers) then
				data.metadata = data.metadata or {}
				data.metadata.timers = timers
			end
		end
	end

	return data
end

camps.load_area = function(pos1, pos2, data)
	local rc = camps.range_vecs(pos1, pos2)
	pos1 = rc.min
	pos2 = rc.max

    local j = 1
    for z=pos1.z,pos2.z do
        for y=pos1.y,pos2.y do
            for x=pos1.x,pos2.x do
                local node = minetest.get_node_or_nil({x=x, y=y, z=z})
                node.name = data.nodes[j]
                node.param1 = data.param1[j]
                node.param2 = data.param2[j]
                j = j + 1
            end
        end
    end

	-- deserialize metadata
	if data.metadata and data.metadata.meta then
		for pos_str, md in pairs(data.metadata.meta) do
			local relative_pos = minetest.string_to_pos(pos_str)
			local absolute_pos = vector.add(pos1, relative_pos)
			local meta = minetest.get_meta(absolute_pos)
			meta:from_table(md)
		end
	end

	-- deserialize node timers
	if data.metadata and data.metadata.timers then
		for pos_str, timer_data in pairs(data.metadata.timers) do
			local relative_pos = minetest.string_to_pos(pos_str)
			local absolute_pos = vector.add(pos1, relative_pos)
			minetest.get_node_timer(absolute_pos):set(timer_data.timeout, timer_data.elapsed)
		end
	end
end
