
-- This allows for a flat good base/foundation to build your campsite on
minetest.register_node("camps:base", {
    short_description = "Foundation",
    description = "Foundation\nYou should not use this node, it is used internally to build a indestructable base for campsites.",
    tiles = {"camps_base.png"},
    groups = {
        not_in_creative_inventory = 1,
    },
    light_source = camps.settings.base_light
})
