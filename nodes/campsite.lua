
--[[
TODO:
  - Add on_punchnode or on_punch callbacks for campsites to "packup"

Please see items/campsites.lua for how the campsites will work on "packing up" or "placing down".
]]

camps.packup_camp = function (pos, node, puncher)
    local tier = 1
    if node.name == "camps:site_gold" then
        tier = 2
    elseif node.name == "camps:site_diamond" then
        tier = 3
    elseif node.name == "camps:site_mese" then
        tier = 4
    end
    local tier_parts = string.split(node.name, "_")
    if #tier_parts < 2 then
        camps.log({func = "packup_camp", param = {pos = pos, node = node.name, puncher = puncher:get_player_name()}, stack = {tier_parts = tier_parts}, err = "not enough tier_parts, need at least 2 but got " .. #tier_parts})
        return
    end
    local meta = minetest.get_meta(pos)
    if meta:get_string("camps:code") == "" then
        -- No code, so just delete the unbreakable stuff (base/foundation, this node)
        local pos1 = vector.subtract(pos, {x = camps.get_range_from_tier(tier), y = 1, z = camps.get_range_from_tier(tier)})
        local pos2 = vector.add(pos, {x = camps.get_range_from_tier(tier), y = -1, z = camps.get_range_from_tier(tier)})
        camps.remove_area(pos1, pos2)
        camps.remove_area(pos, pos)
        minetest.chat_send_player(puncher:get_player_name(), "Campsite had no codex")
    else
        -- Check the code has a terrain if so, then we can restore
        local codebase = minetest.deserialize(camps.store:get_string("codebase")) or {}
        local c = codebase[meta:get_string("camps:code")]
        if c == nil then
            -- Invalid code, we need to clean up
            local pos1 = vector.subtract(pos, {x = camps.get_range_from_tier(tier), y = 1, z = camps.get_range_from_tier(tier)})
            local pos2 = vector.add(pos, {x = camps.get_range_from_tier(tier), y = -1, z = camps.get_range_from_tier(tier)})
            camps.remove_area(pos1, pos2)
            camps.remove_area(pos, pos)
            minetest.chat_send_player(puncher:get_player_name(), "Campsite had invalid codex")
        else
            if c.terrain == nil then
                -- No terrain, we need to clean up
                local pos1 = vector.subtract(pos, {x = camps.get_range_from_tier(tier), y = 1, z = camps.get_range_from_tier(tier)})
                local pos2 = vector.add(pos, {x = camps.get_range_from_tier(tier), y = -1, z = camps.get_range_from_tier(tier)})
                camps.remove_area(pos1, pos2)
                camps.remove_area(pos, pos)
                minetest.chat_send_player(puncher:get_player_name(), "Campsite had invalid terrain record")
            else
                -- Get site
                local data = c.terrain
                camps.log(data)
                local pos1 = vector.subtract(pos, {x = camps.get_range_from_tier(tier), y = 1, z = camps.get_range_from_tier(tier)})
                local pos2 = vector.add(pos, {x = camps.get_range_from_tier(tier), y = camps.get_range_from_tier(tier), z = camps.get_range_from_tier(tier)})
                local old = camps.save_area(pos1, pos2) -- Obtain existing "camp"/site
                camps.remove_area(pos1, pos2) -- wipe
                camps.load_area(pos1, pos2, data) -- place original terrain back
                c.site = old
                codebase[meta:get_string("camps:code")] = c
                camps.store:set_string("codebase", minetest.serialize(codebase))
            end
        end
    end
    local inv = puncher:get_inventory()
    if not inv then
        camps.log({func = "packup_camp", param = {pos = pos, node = node.name, puncher = puncher:get_player_name()}, err = "Player inventory is nil"})
        return
    end
    local i = ItemStack("camps:site_" .. tier_parts[2] .. " 1")
    local imeta = i:get_meta()
    imeta:set_string("camps:code", meta:get_string("camps:code"))
    if inv:room_for_item("main", i) then
        local left = inv:add_item("main", i)
        if left ~= nil then
            camps.log({func = "packup_camp", param = {pos = pos, node = node.name, puncher = puncher:get_player_name()}, stack = {left = left:to_string()}, err = "inventory 'main' for player returned leftover"})
            return
        end
    end
end

minetest.register_node("camps:campsite_iron", {
    short_description = "Campsite (Iron)",
    description = "Campsite (Iron)\nYou should not use this node, it is used internally to build a indestructable base for campsites.",
    tiles = {
        {
            name = "camps_campsite_iron.png",
            animation = {
                type = "vertical_frames",

                aspect_w = 16,
                -- Width of a frame in pixels

                aspect_h = 16,
                -- Height of a frame in pixels

                length = 2.0,
                -- Full loop length
            }
        }
    },
    groups = {
        not_in_creative_inventory = 1,
    },
    light_source = camps.settings.base_light,
    stack_max = 1,
    on_punch = camps.packup_camp
})

minetest.register_node("camps:campsite_gold", {
    short_description = "Campsite (Gold)",
    description = "Campsite (Gold)\nYou should not use this node, it is used internally to build a indestructable base for campsites.",
    tiles = {
        {
            name = "camps_campsite_gold.png",
            animation = {
                type = "vertical_frames",

                aspect_w = 16,
                -- Width of a frame in pixels

                aspect_h = 16,
                -- Height of a frame in pixels

                length = 2.0,
                -- Full loop length
            }
        }
    },
    groups = {
        not_in_creative_inventory = 1,
    },
    light_source = camps.settings.base_light,
    stack_max = 1,
    on_punch = camps.packup_camp
})

minetest.register_node("camps:campsite_diamond", {
    short_description = "Campsite (Diamond)",
    description = "Campsite (Diamond)\nYou should not use this node, it is used internally to build a indestructable base for campsites.",
    tiles = {
        {
            name = "camps_campsite_diamond.png",
            animation = {
                type = "vertical_frames",

                aspect_w = 16,
                -- Width of a frame in pixels

                aspect_h = 16,
                -- Height of a frame in pixels

                length = 2.0,
                -- Full loop length
            }
        }
    },
    groups = {
        not_in_creative_inventory = 1,
    },
    light_source = camps.settings.base_light,
    stack_max = 1,
    on_punch = camps.packup_camp
})

minetest.register_node("camps:campsite_mese", {
    short_description = "Campsite (Mese)",
    description = "Campsite (Mese)\nYou should not use this node, it is used internally to build a indestructable base for campsites.",
    tiles = {
        {
            name = "camps_campsite_mese.png",
            animation = {
                type = "vertical_frames",

                aspect_w = 16,
                -- Width of a frame in pixels

                aspect_h = 16,
                -- Height of a frame in pixels

                length = 2.0,
                -- Full loop length
            }
        }
    },
    groups = {
        not_in_creative_inventory = 1,
    },
    light_source = camps.settings.base_light,
    stack_max = 1,
    on_punch = camps.packup_camp
})

-- There is no crafting of this node, it's only here for how to "packup" a camp.
