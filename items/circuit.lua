
-- iron
minetest.register_craftitem("camps:circuit1", {
    short_description = "Campsite Circuit Board",
    description = "Campsite Circuit Board\nUsed to craft a Campsite\nCPU Specs:\n  Cores: 1\nRAM Specs:\n  RAM: " .. camps.calc_ram(1),
    inventory_image = "camps_circuit1.png",
})

-- gold
minetest.register_craftitem("camps:circuit2", {
    short_description = "Campsite Circuit Board",
    description = "Campsite Circuit Board\nUsed to craft a Campsite\nCPU Specs:\n  Cores: 2\nRAM Specs:\n  RAM: " .. camps.calc_ram(2),
    inventory_image = "camps_circuit2.png",
})

-- diamond
minetest.register_craftitem("camps:circuit3", {
    short_description = "Campsite Circuit Board",
    description = "Campsite Circuit Board\nUsed to craft a Campsite\nCPU Specs:\n  Cores: 2\nRAM Specs:\n  RAM: " .. camps.calc_ram(3),
    inventory_image = "camps_circuit2.png",
})

-- mese
minetest.register_craftitem("camps:circuit4", {
    short_description = "Campsite Circuit Board",
    description = "Campsite Circuit Board\nUsed to craft a Campsite\nCPU Specs:\n  Cores: 4\nRAM Specs:\n  RAM: " .. camps.calc_ram(4),
    inventory_image = "camps_circuit4.png",
})

if camps.settings.enable_crafting then
    local air = ""
    local iron = "default:steel_ingot"
    local gold = "default:gold_ingot"
    local diamond = "default:diamond"
    local mese = "default:mese_crystal" -- swaps with redstone
    local b_iron = "default:iron_block"
    local b_gold = "default:gold_block"
    local b_diamond = "default:diamond_block"
    local b_mese = "default:mese"

    if camps.GAMEMODE == "MCL" then
        iron = "mcl_core:iron_ingot"
        gold = "mcl_core:gold_ingot"
        diamond = "mcl_core:diamond"
        mese = "mcl_core:redstone"
        b_iron = "mcl_core:iron_block"
        b_gold = "mcl_core:gold_block"
        b_diamond = "mcl_core:diamond_block"
        b_mese = "mcl_core:redstone_block"
    end

    -- All parts are compounding (regardless if the craftable_upgrades is true)
    minetest.register_craft({
        output = "camps:circuit1 1",
        recipe = {
            {air, iron, iron},
            {mese, b_iron, iron},
            {air, iron, iron}
        }
    })
    minetest.register_craft({
        output = "camps:circuit2 1",
        recipe = {
            {air, gold, gold},
            {mese, "camps:circuit1", b_gold},
            {air, gold, gold}
        }
    })
    minetest.register_craft({
        output = "camps:circuit3 1",
        recipe = {
            {air, diamond, diamond},
            {mese, "camps:circuit2", b_diamond},
            {air, diamond, diamond}
        }
    })
    minetest.register_craft({
        output = "camps:circuit4 1",
        recipe = {
            {air, b_diamond, b_diamond},
            {b_mese, "camps:circuit3", diamond},
            {air, b_diamond, b_diamond}
        }
    })
end
