
-- iron
minetest.register_craftitem("camps:fab1", {
    short_description = "Campsite Fabricator",
    description = "Campsite Fabricator\nUsed to craft a Campsite\nGenerate Range: " .. camps.settings.iron_camp_range,
    inventory_image = "camps_fab1.png",
})

-- gold
minetest.register_craftitem("camps:fab2", {
    short_description = "Campsite Fabricator",
    description = "Campsite Fabricator\nUsed to craft a Campsite\nGenerate Range: " .. camps.settings.gold_camp_range,
    inventory_image = "camps_fab2.png",
})

-- diamond
minetest.register_craftitem("camps:fab3", {
    short_description = "Campsite Fabricator",
    description = "Campsite Fabricator\nUsed to craft a Campsite\nGenerate Range: " .. camps.settings.diamond_camp_range,
    inventory_image = "camps_fab3.png",
})

-- mese
minetest.register_craftitem("camps:fab4", {
    short_description = "Campsite Fabricator",
    description = "Campsite Fabricator\nUsed to craft a Campsite\nGenerate Range: " .. camps.settings.mese_camp_range,
    inventory_image = "camps_fab4.png",
})

if camps.settings.enable_crafting then
    local air = ""
    local iron = "default:steel_ingot"
    local gold = "default:gold_ingot"
    local diamond = "default:diamond"
    local mese = "default:mese_crystal" -- swaps with redstone
    local b_iron = "default:iron_block"
    local b_gold = "default:gold_block"
    local b_diamond = "default:diamond_block"
    local b_mese = "default:mese"

    if camps.GAMEMODE == "MCL" then
        iron = "mcl_core:iron_ingot"
        gold = "mcl_core:gold_ingot"
        diamond = "mcl_core:diamond"
        mese = "mcl_core:redstone"
        b_iron = "mcl_core:iron_block"
        b_gold = "mcl_core:gold_block"
        b_diamond = "mcl_core:diamond_block"
        b_mese = "mcl_core:redstone_block"
    end

    -- All parts are compounding (regardless if the craftable_upgrades is true)
    minetest.register_craft({
        output = "camps:fab1 1",
        recipe = {
            {iron, air, iron},
            {mese, b_iron, iron},
            {iron, iron, iron}
        }
    })
    minetest.register_craft({
        output = "camps:fab2 1",
        recipe = {
            {gold, air, gold},
            {mese, "camps:fab1", gold},
            {gold, gold, gold}
        }
    })
    minetest.register_craft({
        output = "camps:fab3 1",
        recipe = {
            {diamond, air, diamond},
            {mese, "camps:fab2", diamond},
            {diamond, diamond, diamond}
        }
    })
    minetest.register_craft({
        output = "camps:fab4 1",
        recipe = {
            {b_diamond, air, b_diamond},
            {b_mese, "camps:fab3", b_diamond},
            {b_diamond, b_diamond, b_diamond}
        }
    })
end
