
--[[
TODO:
  - Need to add on_place or on_right_click functions which check if a metadata "camp" was saved
  - If no metadata "camp" is saved we need to set the base camp

On placing a camp: (on_place, on_secondary_place)
1. Save existing area (for when we "packup the campsite")
2. Fill existing area with air (use remove_node to clear meta)
3. Load saved camp from metadata on item
4. Save old area into campsite node

The below is for the node campsites ("camps:campsite_<tier>")
Which will be located in nodes/campsite.lua
On removing a camp: (on_punchnode, on_punch)
1. Save camp area (for when we want to "deploy the campsite")
2. Grab the old area that was here where the camp was (for step 4)
3. Fill existing area with air (use remove_node to clear meta)
4. Fill in the old area where the campsite was
]]

camps.place_camp = function (istack, placer)
    if not placer then
        camps.log({func = "place_camp", param = {istack = istack:to_string(), placer = "nil"}, err = "placer is nil"})
        return
    end
    local name = istack:get_name()
    local tier = 1
    if name == "camps:site_gold" then
        tier = 2
    elseif name == "camps:site_diamond" then
        tier = 3
    elseif name == "camps:site_mese" then
        tier = 4
    end
    local tier_parts = string.split(name, "_")
    if #tier_parts < 2 then
        camps.log({func = "place_camp", param = {istack = istack:to_string(), placer = placer:get_player_name()}, stack = {tier_parts = tier_parts}, err = "not enough tier_parts, need at least 2 but got " .. #tier_parts})
        return
    end
    local pos = placer:get_pos() -- Position to use for placement
    local pos1 = vector.subtract(pos, {x = camps.get_range_from_tier(tier), y = 1, z = camps.get_range_from_tier(tier)})
    local pos2 = vector.add(pos, {x = camps.get_range_from_tier(tier), y = camps.get_range_from_tier(tier), z = camps.get_range_from_tier(tier)})
    local meta = istack:get_meta()
    if meta:get_string("camps:code") == "" then
        -- need to place a new base site
        local old = camps.save_area(pos1, pos2) -- Save existing
        camps.remove_area(pos1, pos2) -- Clear existing
        camps.replace_in_area(nil, "camps:base", pos1, {x = pos.x+camps.get_range_from_tier(tier), y = pos.y-1, z = pos.z+camps.get_range_from_tier(tier)})
        minetest.set_node(vector.subtract(pos, {x = 0, y = 1, z = 0}), {name="camps:campsite_" .. tier_parts[2]})
        local base = minetest.get_meta(vector.subtract(pos, {x = 0, y = 1, z = 0}))
        local code = camps.generate_code()
        base:set_string("camps:code", code)
        --base:set_string("camps:area", minetest.serialize(old))
        local codebase = minetest.deserialize(camps.store:get_string("codebase")) or {}
        local c = codebase[code]
        if c == nil then
            c = {
                site = nil,
                terrain = old
            }
        else
            c.terrain = old
        end
        codebase[code] = c
        camps.store:set_string("codebase", minetest.serialize(codebase))
    else
        -- unpack site data
        local codebase = minetest.deserialize(camps.store:get_string("codebase")) or {}
        local data
        local c = codebase[meta:get_string("camps:code")]
        if c ~= nil then
            if c.site == nil then
                camps.log({func = "place_camp", param = {istack = istack:to_string(), placer = placer:get_player_name()}, stack = {c = c, code = meta:get_string("camps:code")}, err = "code given has nil site data"})
                return
            end
            data = c.site
        else
            camps.log({func = "place_camp", param = {istack = istack:to_string(), placer = placer:get_player_name()}, stack = {c = "nil", code = meta:get_string("camps:code")}, err = "code given is nil"})
            return
        end
        -- save existing data
        local old = camps.save_area(pos1, pos2)
        -- Erase existing and place data
        camps.remove_area(pos1, pos2)
        camps.load_area(pos1, pos2, data)
        -- Add/update base of original area
        local base = minetest.get_meta(vector.subtract(pos, {x = 0, y = 1, z = 0}))
        --base:set_string("camps:area", minetest.serialize(old))
        base:set_string("camps:code", meta:get_string("camps:code"))
        c.terrain = old
        codebase[meta:get_string("camps:code")] = c
        camps.store:set_string("codebase", minetest.serialize(codebase))
    end
    istack:take_item(1)
    return istack
end

-- iron
minetest.register_craftitem("camps:site_iron", {
    short_description = "Campsite",
    description = "Campsite\nPlace (Right Click) to access the camp\nSize: " .. camps.settings.iron_camp_range .. " (" .. camps.calc_blocks(1) .. " blocks)",
    inventory_image = minetest.inventorycube("camps_campsite_iron.png"),
    stack_max = 1,
    on_place = camps.place_camp,
    on_secondary_place = camps.place_camp
})

-- gold
minetest.register_craftitem("camps:site_gold", {
    short_description = "Campsite",
    description = "Campsite\nPlace (Right Click) to access the camp\nSize: " .. camps.settings.gold_camp_range .. " (" .. camps.calc_blocks(2) .. " blocks)",
    inventory_image = minetest.inventorycube("camps_campsite_gold.png"),
    stack_max = 1,
    on_place = camps.place_camp,
    on_secondary_place = camps.place_camp
})

-- diamond
minetest.register_craftitem("camps:site_diamond", {
    short_description = "Campsite",
    description = "Campsite\nPlace (Right Click) to access the camp\nSize: " .. camps.settings.diamond_camp_range .. " (" .. camps.calc_blocks(3) .. " blocks)",
    inventory_image = minetest.inventorycube("camps_campsite_diamond.png"),
    stack_max = 1,
    on_place = camps.place_camp,
    on_secondary_place = camps.place_camp
})

-- mese
minetest.register_craftitem("camps:site_mese", {
    short_description = "Campsite",
    description = "Campsite\nPlace (Right Click) to access the camp\nSize: " .. camps.settings.mese_camp_range .. " (" .. camps.calc_blocks(4) .. " blocks)",
    inventory_image = minetest.inventorycube("camps_campsite_mese.png"),
    stack_max = 1,
    on_place = camps.place_camp,
    on_secondary_place = camps.place_camp
})

-- There will be no upgrading if crafting is disabled too
if camps.settings.enable_crafting then
    local air = ""
    local iron = "default:steel_ingot"
    local gold = "default:gold_ingot"
    local diamond = "default:diamond"
    local mese = "default:mese_crystal" -- swaps with redstone
    local b_iron = "default:iron_block"
    local b_gold = "default:gold_block"
    local b_diamond = "default:diamond_block"
    local b_mese = "default:mese"

    if camps.GAMEMODE == "MCL" then
        iron = "mcl_core:iron_ingot"
        gold = "mcl_core:gold_ingot"
        diamond = "mcl_core:diamond"
        mese = "mcl_core:redstone"
        b_iron = "mcl_core:iron_block"
        b_gold = "mcl_core:gold_block"
        b_diamond = "mcl_core:diamond_block"
        b_mese = "mcl_core:redstone_block"
    end

    local circuit1 = "camps:circuit1"
    local circuit2 = "camps:circuit2"
    local circuit3 = "camps:circuit3"
    local circuit4 = "camps:circuit4"
    local fab1 = "camps:fab1"
    local fab2 = "camps:fab2"
    local fab3 = "camps:fab3"
    local fab4 = "camps:fab4"
    local compact1 = "camps:compact1"
    local compact2 = "camps:compact2"
    local compact3 = "camps:compact3"
    local compact4 = "camps:compact4"

    -- Regardless of the craftable_upgrades option the first tier will be avalible either way
    minetest.register_craft({
        type = "shapeless",
        output = "camps:site_iron",
        recipe = {
            circuit1,
            circuit1,
            fab1,
            compact1
        }
    })

    if camps.settings.craftable_upgrades then
        -- Each tier is dependent, you must make the previous tier campsite in order to make the higher tier
        -- Since each are dependent I reduced the number of circuits needed to just 1
        minetest.register_craft({
            type = "shapeless",
            output = "camps:site_gold",
            recipe = {
                circuit2,
                fab2,
                compact2,
                "camps:site_iron"
            }
        })
        minetest.register_craft({
            type = "shapeless",
            output = "camps:site_diamond",
            recipe = {
                circuit3,
                fab3,
                compact3,
                "camps:site_gold"
            }
        })
        minetest.register_craft({
            type = "shapeless",
            output = "camps:site_mese",
            recipe = {
                circuit4,
                fab4,
                compact4,
                "camps:site_diamond"
            }
        })
    else
        -- Each tier is separate, you do not need to make a campsite of a lower tier to make the higher tier
        minetest.register_craft({
            type = "shapeless",
            output = "camps:site_gold",
            recipe = {
                circuit2,
                circuit2,
                fab2,
                compact2
            }
        })
        minetest.register_craft({
            type = "shapeless",
            output = "camps:site_diamond",
            recipe = {
                circuit3,
                circuit3,
                fab3,
                compact3
            }
        })
        minetest.register_craft({
            type = "shapeless",
            output = "camps:site_mese",
            recipe = {
                circuit4,
                circuit4,
                fab4,
                compact4
            }
        })
    end
end
