
-- iron
minetest.register_craftitem("camps:compact1", {
    short_description = "Campsite Compactor",
    description = "Campsite Compactor\nUsed to craft a Campsite\nCompression Level: " .. camps.settings.iron_camp_range,
    inventory_image = "camps_compact1.png",
})

-- gold
minetest.register_craftitem("camps:compact2", {
    short_description = "Campsite Compactor",
    description = "Campsite Compactor\nUsed to craft a Campsite\nCompression Level: " .. camps.settings.gold_camp_range,
    inventory_image = "camps_compact2.png",
})

-- diamond
minetest.register_craftitem("camps:compact3", {
    short_description = "Campsite Compactor",
    description = "Campsite Compactor\nUsed to craft a Campsite\nCompression Level: " .. camps.settings.diamond_camp_range,
    inventory_image = "camps_compact3.png",
})

-- mese
minetest.register_craftitem("camps:compact4", {
    short_description = "Campsite Compactor",
    description = "Campsite Compactor\nUsed to craft a Campsite\nCompression Level: " .. camps.settings.mese_camp_range,
    inventory_image = "camps_compact4.png",
})

if camps.settings.enable_crafting then
    local air = ""
    local iron = "default:steel_ingot"
    local gold = "default:gold_ingot"
    local diamond = "default:diamond"
    local mese = "default:mese_crystal" -- swaps with redstone
    local b_iron = "default:iron_block"
    local b_gold = "default:gold_block"
    local b_diamond = "default:diamond_block"
    local b_mese = "default:mese"

    if camps.GAMEMODE == "MCL" then
        iron = "mcl_core:iron_ingot"
        gold = "mcl_core:gold_ingot"
        diamond = "mcl_core:diamond"
        mese = "mcl_core:redstone"
        b_iron = "mcl_core:iron_block"
        b_gold = "mcl_core:gold_block"
        b_diamond = "mcl_core:diamond_block"
        b_mese = "mcl_core:redstone_block"
    end

    -- All parts are compounding (regardless if the craftable_upgrades is true)
    minetest.register_craft({
        output = "camps:compact1 1",
        recipe = {
            {iron, b_iron, iron},
            {iron, air, mese},
            {iron, b_iron, iron}
        }
    })
    minetest.register_craft({
        output = "camps:compact2 1",
        recipe = {
            {gold, b_gold, gold},
            {gold, "camps:compact1", mese},
            {gold, b_gold, gold}
        }
    })
    minetest.register_craft({
        output = "camps:compact3 1",
        recipe = {
            {diamond, b_diamond, diamond},
            {diamond, "camps:compact2", mese},
            {diamond, b_diamond, diamond}
        }
    })
    minetest.register_craft({
        output = "camps:compact4 1",
        recipe = {
            {diamond, b_diamond, diamond},
            {diamond, "camps:compact3", b_mese},
            {diamond, b_diamond, diamond}
        }
    })
end
