
camps = {
    VERSION = {
        id = "1.0-dev",
        name = "Yellowstone"
    },
    GAMEMODE = "???",
    settings = {
        debug = true, -- Print out pos and what not
        enable_crafting = true, -- Toggles if you can craft a camp, or if they are given by admin or shops only.
        craftable_upgrades = true, -- This toggles if you use the previous camp level in the next level camp (iron is needed to make gold, gold needed to make diamond), else there is a separate crafting recipe for each level
        -- Ranges are from the base/root node in a single direction
        iron_camp_range = 7, -- 15x15x15 (3149 usable blocks)
        gold_camp_range = 9, -- 19x19x19 (6497 usable blocks)
        diamond_camp_range = 12, -- 25x25x25 (14999 usable blocks)
        mese_camp_range = 16, -- 33x33x33 (34847 usable blocks)
        base_light = 4, -- Base/Foundation nodes of a camp can emit light (set to 0 to not allow it to emit light)
    },
    store = minetest.get_mod_storage(),
    log = function (msg)
        if type(msg) == "table" then
            msg = minetest.serialize(msg)
        end
        minetest.log("action", "[camps] " .. msg)
    end,
    modpath = minetest.get_modpath("camps")
}

if camps.store:get_string("codebase") == "" then
    camps.store:set_string("codebase", minetest.serialize({}))
end

camps.log("Version: " .. camps.VERSION.name .. " (" .. camps.VERSION.id .. ")")
camps.log("Lua Version: " .. _VERSION)
camps.log("Detecting gamemode...")
if minetest.get_modpath("default") then
    camps.GAMEMODE = "MTG"
elseif minetest.get_modpath("mcl_core") then
    camps.GAMEMODE = "MCL"
end
camps.log("Gamemode: " .. camps.GAMEMODE)

dofile(camps.modpath .. DIR_DELIM .. "utils" .. DIR_DELIM .. "init.lua")
dofile(camps.modpath .. DIR_DELIM .. "api" .. DIR_DELIM .. "init.lua")
dofile(camps.modpath .. DIR_DELIM .. "items" .. DIR_DELIM .. "init.lua")
dofile(camps.modpath .. DIR_DELIM .. "nodes" .. DIR_DELIM .. "init.lua")
