
dofile(camps.modpath .. DIR_DELIM .. "utils" .. DIR_DELIM .. "strings.lua")

camps.pos2str = function (pos)
    if not pos then
        camps.log({func = "pos2str", param = {pos = "nil"}, err = "pos is nil"})
        return
    end
    if pos.x == nil or pos.y == nil or pos.z == nil then
        camps.log({func = "pos2str", param = {pos = pos}, err = "pos does not have 'x', 'y', or 'z'"})
        return
    end
    return string.format("%.1f %.1f %.1f", pos.x, pos.y, pos.z)
end

camps.str2pos = function (str)
    if not str then
        camps.log({func = "str2pos", param = {str = "nil"}, err = "str is nil"})
        return
    end
    if str:len() == 0 then
        camps.log({func = "str2pos", param = {str = str}, err = "str is 0 length"})
        return
    end
    local p = {x = 0.0, y = 0.0, z = 0.0}
    local parts = string.split(str)
    if #parts < 3 then
        camps.log({func = "str2pos", param = {str = str}, stack = {parts = parts}, err = "str doesn't have enough spaces for 3 parts"})
        return
    end
    local x = tonumber(parts[1])
    local y = tonumber(parts[2])
    local z = tonumber(parts[3])

    if not x or not y or not z then
        camps.log({func = "str2pos", param = {str = str}, stack = {parts = parts, x = x, y = y, z = z}, err = "str parts not convertable to number"})
        return
    end

    p.x = x
    p.y = y
    p.z = z
    return p
end

camps.calc_ram = function (tier)
    if not tier then
        camps.log({func = "calc_ram", param = {tier = "nil"}, err = "tier is nil"})
        return "0 MB"
    end
    if tier == 1 then
        local mb = camps.settings.iron_camp_range * camps.settings.iron_camp_range
        return mb .. " MB"
    elseif tier == 2 then
        local mb = camps.settings.gold_camp_range * camps.settings.gold_camp_range
        return mb .. " MB"
    elseif tier == 3 then
        local mb = camps.settings.diamond_camp_range * camps.settings.diamond_camp_range
        return mb .. " MB"
    elseif tier == 4 then
        local mb = camps.settings.mese_camp_range * camps.settings.mese_camp_range
        return mb .. " MB"
    else
        camps.log({func = "calc_ram", param = {tier = tier}, err = "tier not within 1-4"})
        return "0 MB"
    end
end

camps.get_range_from_tier = function (tier)
    if not tier then
        camps.log({func = "get_range_from_tier", param = {tier = "nil"}, err = "tier is nil"})
        return 0
    end
    if tier == 1 then
        return camps.settings.iron_camp_range
    elseif tier == 2 then
        return camps.settings.gold_camp_range
    elseif tier == 3 then
        return camps.settings.diamond_camp_range
    elseif tier == 4 then
        return camps.settings.mese_camp_range
    else
        camps.log({func = "get_range_from_tier", param = {tier = tier}, err = "tier not within 1-4"})
        return 0
    end
end

camps.calc_blocks = function (tier)
    if not tier then
        camps.log({func = "calc_blocks", param = {tier = "nil"}, err = "tier is nil"})
        return 0
    end
    if tier == 1 then
        local tmp = (camps.settings.iron_camp_range+1) * camps.settings.iron_camp_range * (camps.settings.iron_camp_range+1)
        return tmp-1
    elseif tier == 2 then
        local tmp = (camps.settings.gold_camp_range+1) * camps.settings.gold_camp_range * (camps.settings.gold_camp_range+1)
        return tmp-1
    elseif tier == 3 then
        local tmp = (camps.settings.diamond_camp_range+1) * camps.settings.diamond_camp_range * (camps.settings.diamond_camp_range+1)
        return tmp-1
    elseif tier == 4 then
        local tmp = (camps.settings.mese_camp_range+1) * camps.settings.mese_camp_range * (camps.settings.mese_camp_range+1)
        return tmp-1
    else
        camps.log({func = "calc_blocks", param = {tier = tier}, err = "tier not within 1-4"})
        return 0
    end
end

-- Generates the minimum and maximum vec3s given 2 vec3s
camps.range_vecs = function (pos1, pos2)
    local i = {}
    local ii = {}
    if pos1.x > pos2.x then
        i.x = pos2.x
        ii.x = pos1.x
    else
        i.x = pos1.x
        ii.x = pos2.x
    end
    if pos1.y > pos2.y then
        i.y = pos2.y
        ii.y = pos1.y
    else
        i.y = pos1.y
        ii.y = pos2.y
    end
    if pos1.z > pos2.z then
        i.z = pos2.z
        ii.z = pos1.z
    else
        i.z = pos1.z
        ii.z = pos2.z
    end
    return {min=i, max=ii}
end

local CODEX = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"}

camps.generate_code = function ()
    local code = ""
    for i = 0, 12 do
        code = code .. CODEX[math.random(#CODEX)]
    end
    camps.log({func = "generate_code", returns = {code = code}})
    return code
end

---Replaces nodes within an area, uses a Voxel Manipulator for speed.
---
---from can be nil to replace all nodes within the given pos1 to pos2
---
---to can be a table to pick a random node within to to replace the node
---@param from string
---@param to string
---@param pos1 vector
---@param pos2 vector
camps.replace_in_area = function (from, to, pos1, pos2)
    local rc = camps.range_vecs(pos1, pos2)
    local min = rc.min
    local max = rc.max

    local c_to
    local c_from
    if type(to) == "table" then
        c_to = {}
        for _, n in ipairs(to) do
            table.insert(c_to, minetest.get_content_id(n))
        end
    else
        c_to = minetest.get_content_id(to)
    end
    if from ~= nil then
        c_from = minetest.get_content_id(from)
    end

    for z = min.z, max.z do
        for y = min.y, max.y do
            for x = min.x, max.x do
                local pos = {x=x, y=y, z=z}
                local n = minetest.get_node_or_nil(pos)
                if c_from ~= nil and n ~= nil then
                    -- Replaces from with either any node within to or just to
                    if n.name == c_from then
                        if type(c_to) == "table" then
                            minetest.set_node(pos, c_to[math.random(#c_to)])
                        else
                            minetest.set_node(pos, c_to)
                        end
                    end
                else
                    -- Replaces any with either any node within to or just to
                    if type(c_to) == "table" then
                        minetest.set_node(pos, c_to[math.random(#c_to)])
                    else
                        minetest.set_node(pos, c_to)
                    end
                end
            end
        end
    end
end

-- Wipes the area out (removes metadata)
camps.remove_area = function (pos1, pos2)
    local rc = camps.range_vecs(pos1, pos2)
    local min = rc.min
    local max = rc.max

    for z = min.z, max.z do
        for y = min.y, max.y do
            for x = min.x, max.x do
                minetest.remove_node(vector.new(x, y, z))
            end
        end
    end
end
