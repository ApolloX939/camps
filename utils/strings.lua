
---Splits the given line by seperator
---@param inputstr string
---@param sep string? Defaults to spaces if left empty/nil
---@return table
string.split = function (inputstr, sep)
    if sep == nil then
        sep = "%s"
    end
    local t={}
    for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
        table.insert(t, str)
    end
    return t
end
